package com.example.solarcalculator

import androidx.lifecycle.*
import com.example.solarcalculator.models.PlaceData
import com.example.solarcalculator.repos.PlaceRepo
import com.google.android.gms.maps.model.LatLng
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class MapsViewModel(val placeRepo: PlaceRepo, val calc: PhaseTimeCalc) : ViewModel() {
    private val _data = MutableLiveData<ArrayList<PlaceData>>().apply { value = ArrayList() }
    private val _errMsg = MutableLiveData<String>()
    private val _selectedPlace = MutableLiveData<PlaceData>().apply { value = null }
    private val _selectedDate =
        MutableLiveData<Date>().apply {
            val c = Calendar.getInstance()
            value = c.time
        }

    private val placeDetails = HashMap<String, PlaceData>()

    val data: LiveData<ArrayList<PlaceData>>
        get() = _data

    val selectedPlace: LiveData<PlaceData>
        get() = _selectedPlace

    val selectedDate: LiveData<Date>
        get() = _selectedDate

    val errMsg: LiveData<String>
        get() = _errMsg

    val phaseTimeCalc: MediatorLiveData<PhaseTimeCalc> =
        MediatorLiveData<PhaseTimeCalc>().apply {
            addSource(_selectedDate) { date ->
                val place = _selectedPlace.value
                if (date != null && place != null) {
                    this.value = calc.calculate(date, place.latLng)
                }
            }
            addSource(_selectedPlace) { place ->
                val date = _selectedDate.value
                if (place != null && date != null) {
                    this.value = calc.calculate(date, place.latLng)
                }
            }
        }

    fun getPlacesByName(query: String) {
        placeRepo.getPlacesByName(query, {
            it.forEach { p ->
                placeDetails[p.id] = p
            }
            _data.value = ArrayList(it)
        }, {
            it.printStackTrace()
            _errMsg.value = it.message
        })
    }

    fun getPlaceDataForId(placeId: String) {
        if (placeDetails[placeId]?.isLatLngValid != true)
            placeRepo.getPlaceDetails(placeId, {
                placeDetails[placeId]?.latLng = it
                placeDetails[placeId]?.isLatLngValid = true
                _selectedPlace.value = placeDetails[placeId]
            }, {
                it.printStackTrace()
                _errMsg.value = it.message
            })
        else
            _selectedPlace.value = placeDetails[placeId]
    }

    fun addOrMinusDate(isAdd: Boolean) {
        val cal = Calendar.getInstance()
        cal.time = _selectedDate.value!!
        if (isAdd)
            cal.add(Calendar.DATE, 1)
        else
            cal.add(Calendar.DATE, -1)
        _selectedDate.value = cal.time
    }

    fun resetDate() {
        _selectedDate.value = Calendar.getInstance().time
    }

    fun setCurPlace(latLng: LatLng) {
        val newPlace = PlaceData("", "", "", latLng)
        if (newPlace != _selectedPlace.value) {
            _selectedPlace.value = newPlace
        }
    }

    fun setCurPlace(place: PlaceData) {
        if (place != _selectedPlace.value) {
            _selectedPlace.value = place
        }
    }

    fun saveToFavs(onSuccess: (Boolean) -> Unit) {
        val place = _selectedPlace.value
        if (place != null) {
            placeRepo.saveFav(place, onSuccess)
        }
    }

    fun fetchFavs(onSuccess: (List<PlaceData>) -> Unit) {
        placeRepo.fetchFavs(onSuccess)
    }

    class Factory(private val repo: PlaceRepo, private val calc: PhaseTimeCalc) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return MapsViewModel(repo, calc) as T
        }
    }
}
