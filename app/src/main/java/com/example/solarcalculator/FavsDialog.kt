package com.example.solarcalculator

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.solarcalculator.adapters.FavsAdapter
import com.example.solarcalculator.models.PlaceData

class FavsDialog(
    val ctx: Context,
    val data: ArrayList<PlaceData>,
    val onItemClicked: (PlaceData) -> Unit
) : Dialog(ctx) {

    init {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.requestWindowFeature(android.R.style.Theme_Translucent_NoTitleBar)
        this.setContentView(R.layout.dialog_favs)
        this.window!!.setGravity(Gravity.CENTER)

        val window = this.window
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(window!!.attributes)

        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp

        this.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val rv = findViewById<RecyclerView>(R.id.rv_favs)
        rv.layoutManager = LinearLayoutManager(ctx)
        rv.adapter = FavsAdapter(data) {
            dismiss()
            onItemClicked(it)
        }
    }

}