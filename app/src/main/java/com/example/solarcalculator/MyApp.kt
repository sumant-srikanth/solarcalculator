package com.example.solarcalculator

import android.app.Application
import android.content.Context
import com.example.solarcalculator.db.AppDB

class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        INSTANCE = this

        AppDB.initialize(this)
    }

    companion object {
        private lateinit var INSTANCE: MyApp

        fun getAppContext(): Context {
            return INSTANCE.applicationContext
        }
    }
}