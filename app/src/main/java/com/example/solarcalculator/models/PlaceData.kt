package com.example.solarcalculator.models

import androidx.room.Entity
import androidx.room.Ignore
import com.google.android.gms.maps.model.LatLng

@Entity(tableName = "places", primaryKeys = ["id", "latLng"])
data class PlaceData(
    val id: String,
    val name: String,
    val desc: String,
    var latLng: LatLng
) {
    var created_at: Long = 0

    @Ignore
    var isLatLngValid: Boolean = false
}