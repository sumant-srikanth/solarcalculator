package com.example.solarcalculator.tasks

import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.work.*
import com.example.solarcalculator.AppConstants
import com.example.solarcalculator.NotificationUtil
import com.example.solarcalculator.Util
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class GoldenHourWorker(ctx: Context, workerParameters: WorkerParameters) :
    Worker(ctx, workerParameters) {

    companion object {
        val NAME = "GOLDEN_HOUR_WORKER"

        fun enque(delayMillis: Long) {
            println(">>>>>>>>> WORK === " + delayMillis)

            val workRequest = OneTimeWorkRequest.Builder(GoldenHourWorker::class.java)
                .setInitialDelay(delayMillis, TimeUnit.MILLISECONDS)
                .build()

            val workManager = WorkManager.getInstance()
            workManager.enqueueUniqueWork(NAME, ExistingWorkPolicy.REPLACE, workRequest)
        }
    }

    override fun doWork(): Result {
        val ghTime = Util.getStringPrefs(AppConstants.PREFS_GOLDEN_HOUR_TIME)
        val timestamp = ghTime.toLongOrNull()
        if (timestamp != null) {
            val date = Date(timestamp)
            val sdf = SimpleDateFormat("hh:mm a", Locale.ENGLISH)
            val time = sdf.format(date)

            NotificationUtil.generateNotification(
                applicationContext,
                "Golden Hour",
                "Solar Calculator",
                "Golden Hour will be at ${time} today.",
                NotificationUtil.CHANNEL_ID_GOLDEN_HOUR,
                NotificationUtil.CHANNEL_NAME_GOLDEN_HOUR,
                NotificationUtil.CHANNEL_DESC_GOLDEN_HOUR,
                NotificationCompat.PRIORITY_HIGH
            )
        }

        return Result.success()
    }


}