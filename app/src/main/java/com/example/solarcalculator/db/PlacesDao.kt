package com.example.solarcalculator.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.solarcalculator.models.PlaceData
import androidx.room.Update


@Dao
interface PlacesDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(entity: PlaceData): Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun update(entity: PlaceData)

    @Query("SELECT * FROM places ORDER BY created_at DESC")
    fun getAll(): List<PlaceData>
}