package com.example.solarcalculator.db

import androidx.room.TypeConverter
import com.google.android.gms.maps.model.LatLng

class MyTypeConverters {

    @TypeConverter
    fun appToString(latLng: LatLng): String {
        return "${latLng.latitude},${latLng.longitude}"
    }

    @TypeConverter
    fun stringToApp(string: String): LatLng {
        val arr = string.split(",")
        if (arr.size == 2) {
            val lat = arr[0].toDoubleOrNull() ?: 0.0
            val lng = arr[1].toDoubleOrNull() ?: 0.0
            return LatLng(lat, lng)
        }
        return LatLng(0.0, 0.0)
    }

}