package com.example.solarcalculator.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.solarcalculator.models.PlaceData

@Database(entities = arrayOf(PlaceData::class), version = 1, exportSchema = false)
@TypeConverters(MyTypeConverters::class)
abstract class AppDB : RoomDatabase() {

    abstract fun getPinnedPlacesDao(): PlacesDao

    companion object {
        val DB_NAME = "db_solar_calculator"
        private var appDb: AppDB? = null

        @JvmStatic
        fun initialize(context: Context) {
            appDb = Room.databaseBuilder(context, AppDB::class.java, DB_NAME)
                .addMigrations()
                .fallbackToDestructiveMigration()
                .build()
        }

        @JvmStatic
        @Throws(IllegalStateException::class)
        fun getInstance(): AppDB {
            if (appDb == null) {
                throw IllegalStateException("Database must be initialized!")
            }
            return appDb!!
        }
    }
}