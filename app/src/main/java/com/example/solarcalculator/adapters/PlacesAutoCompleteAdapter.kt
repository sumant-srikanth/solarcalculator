package com.example.solarcalculator.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.solarcalculator.R
import com.example.solarcalculator.models.PlaceData

class PlacesAutoCompleteAdapter(val ctx: Context) :
    ArrayAdapter<String>(ctx, 0) {

    private val data: ArrayList<PlaceData> = ArrayList()

    fun setData(newData: List<PlaceData>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var v = convertView
        val holder: ViewHolder
        if (v == null) {
            v = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_place_item, parent, false) as ViewGroup
            holder = ViewHolder(
                v.getChildAt(0) as TextView,
                v.getChildAt(1) as TextView
            )
            v.tag = holder
        } else {
            holder = v.tag as ViewHolder
        }
        holder.title.setText(data[position].name)
        holder.subtTitle.setText(data[position].desc)
        return v
    }

    inner class ViewHolder(
        val title: TextView,
        val subtTitle: TextView
    )

    override fun getItem(position: Int): String? {
        return data[position].name
    }

    override fun getCount(): Int {
        return data.size
    }

}