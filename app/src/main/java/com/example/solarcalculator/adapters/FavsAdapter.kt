package com.example.solarcalculator.adapters

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.solarcalculator.R
import com.example.solarcalculator.Util
import com.example.solarcalculator.models.PlaceData

class FavsAdapter(val data: ArrayList<PlaceData>, val onItemClicked: (PlaceData) -> Unit) :
    RecyclerView.Adapter<FavsAdapter.ViewHolder>() {

    private lateinit var ctx: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavsAdapter.ViewHolder {
        if (!::ctx.isInitialized)
            ctx = parent.context
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.layout_favs_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: FavsAdapter.ViewHolder, position: Int) {
        val place = data[position]
        if (!TextUtils.isEmpty(place.name)) {
            holder.title.text = place.name
        } else {
            holder.title.text = ctx.getText(R.string.unnamed_loc)
        }
        if (!TextUtils.isEmpty(place.desc)) {
            holder.subtitle.text = place.desc
            holder.subtitle.visibility = View.VISIBLE
        } else {
            holder.subtitle.visibility = View.GONE
        }
        holder.latlng.text = Util.convertLatLngToString(place.latLng)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.title)
        val subtitle: TextView = itemView.findViewById(R.id.subtitle)
        val latlng: TextView = itemView.findViewById(R.id.latlng)

        init {
            itemView.setOnClickListener {
                onItemClicked(data[adapterPosition])
            }
        }
    }

}