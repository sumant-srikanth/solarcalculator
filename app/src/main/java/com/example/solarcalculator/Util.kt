package com.example.solarcalculator

import android.preference.PreferenceManager
import com.google.android.gms.maps.model.LatLng

class Util {
    companion object {

        fun convertLatLngToString(it: LatLng): String {
            return "${String.format("%.5f", it.latitude)}, ${String.format("%.5f", it.longitude)}"
        }

        fun saveStringPrefs(key: String, value: String) {
            val sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(MyApp.getAppContext())
            if (sharedPreferences != null) {
                val editor = sharedPreferences.edit()
                editor.putString(key, value)
                editor.commit()
            }
        }

        fun getStringPrefs(key: String): String {
            val sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(MyApp.getAppContext())
            return sharedPreferences?.getString(key, "") ?: ""
        }
    }
}