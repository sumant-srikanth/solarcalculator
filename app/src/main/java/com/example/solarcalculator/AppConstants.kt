package com.example.solarcalculator

class AppConstants {
    companion object {
        const val PREFS_GOLDEN_HOUR_TIME = "PREFS_GOLDEN_HOUR_TIME"

        const val REMINDER_SECONDS_BEFORE_EVENT = 1 * 60
        const val REMINDER_MIN_SECONDS_BEFORE_EVENT = 10L
    }
}