package com.example.solarcalculator

import com.google.android.gms.maps.model.LatLng
import java.util.*
import kotlin.math.*

class PhaseTimeCalc {
    var sunrise: Double = 0.0 // sunrise
        private set

    var sunset: Double = 0.0 // sunset
        private set

    fun calculate(date: Date, latLng: LatLng): PhaseTimeCalc {
        val cal = Calendar.getInstance()
        cal.time = date
        return calculate(
            cal.get(Calendar.DAY_OF_MONTH),
            cal.get(Calendar.MONTH),
            cal.get(Calendar.YEAR),
            latLng.latitude,
            latLng.longitude
        )
    }

    fun calculate(
        day: Int,
        month: Int,
        year: Int,
        lat: Double,
        lng: Double,
        zenith: Double = 90.0
    ): PhaseTimeCalc {
        val p = this
        val N = p.getDayOfYear(day, month, year)

        val t1 = p.lngToHour(N, lng, true)
        val M1 = p.getMeanAnomaly(t1)
        val L1 = p.getSunTrueLng(M1)
        val RA1 = p.getSunAscensionHrs(L1)
        val sinDec1 = p.getSunSinDeclination(L1)
        val cosDec1 = p.getSunCosDeclination(sinDec1)
        val H1 = p.getSunLocalHrAngle(lat, zenith, sinDec1, cosDec1, true)
        val T1 = p.getLocalMeanTime(H1, RA1, t1)
        p.sunrise = p.getUTC(T1, lng)

        val t2 = p.lngToHour(N, lng, false)
        val M2 = p.getMeanAnomaly(t2)
        val L2 = p.getSunTrueLng(M2)
        val RA2 = p.getSunAscensionHrs(L2)
        val sinDec2 = p.getSunSinDeclination(L2)
        val cosDec2 = p.getSunCosDeclination(sinDec2)
        val H2 = p.getSunLocalHrAngle(lat, zenith, sinDec2, cosDec2, false)
        val T2 = p.getLocalMeanTime(H2, RA2, t2)
        p.sunset = p.getUTC(T2, lng)

        return p
    }

    private fun getDayOfYear(day: Int, month: Int, year: Int): Int {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, day, 0, 0, 0)
        return calendar.get(Calendar.DAY_OF_YEAR)
    }

    private fun lngToHour(dayOfYear: Int, lng: Double, isSunrise: Boolean): Double {
        val lngHour = lng / 15
        if (isSunrise)
            return dayOfYear + ((6 - lngHour) / 24)
        else
            return dayOfYear + ((18 - lngHour) / 24)
    }

    private fun getMeanAnomaly(t: Double): Double {
        return (0.9856 * t) - 3.289
    }

    private fun getSunTrueLng(M: Double): Double {
        return (M + (1.916 * sin(M.toRad())) + (0.02 + sin(2 * M.toRad())) + 282.634) % 360
    }

    private fun getSunAscensionHrs(L: Double): Double {
        val RA = (atan(0.91764 * tan(L.toRad())).toDeg()) % 360
        val lq = (floor(L / 90)) * 90
        val rq = (floor(RA / 90)) * 90
        return (RA + (lq - rq)) / 15
    }

    private fun getSunSinDeclination(L: Double): Double {
        return 0.39782 * sin(L.toRad())
    }

    private fun getSunCosDeclination(sinDec: Double): Double {
        return cos(asin(sinDec))
    }

    private fun getSunLocalHrAngle(
        lat: Double,
        zenith: Double,
        sinDec: Double,
        cosDec: Double,
        isSunrise: Boolean
    ): Double {
        val cosH = (cos(zenith.toRad()) - (sinDec * sin(lat.toRad()))) / (cosDec * cos(
            lat.toRad()
        ))

        if (isSunrise)
            return (360 - acos(cosH).toDeg()) / 15
        return acos(cosH).toDeg() / 15
    }

    private fun getLocalMeanTime(H: Double, RA: Double, t: Double): Double {
        return H + RA - (0.06571 * t) - 6.622
    }

    private fun getUTC(T: Double, lng: Double): Double {
        val utc = (T - lng / 15)
        if (utc > 24)
            return utc - 24
        if (utc < 0)
            return utc + 24
        return utc
    }

    /*
    private fun getLocalTime(UT: Double) : Double {
        return UT + Calendar.lo
    }
     */


    fun Double.toRad(): Double {
        return PI / 180.0 * this
    }

    fun Double.toDeg(): Double {
        return 180.0 / PI * this
    }

}