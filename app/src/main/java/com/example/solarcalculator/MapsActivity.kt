package com.example.solarcalculator

import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.solarcalculator.adapters.PlacesAutoCompleteAdapter
import com.example.solarcalculator.repos.PlaceRepo
import com.example.solarcalculator.tasks.GoldenHourWorker
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_maps.*
import java.text.SimpleDateFormat
import java.util.*


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private var myMarker: Marker? = null
    private lateinit var initLocation: LatLng
    private var isLocationEnabled = false
    private var isGoldenHourReminderSet = false

    private lateinit var adapter: PlacesAutoCompleteAdapter
    private lateinit var viewModel: MapsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        adapter = PlacesAutoCompleteAdapter(this)
        search_bar.setAdapter(adapter)
        search_bar.threshold = 1
        search_bar.doOnTextChanged { text, start, count, after ->
            handler.removeMessages(MESSAGE_AUTO_COMPLETE_TEXT_CHANGED)
            if (search_bar.isPerformingCompletion)
                return@doOnTextChanged
            handler.sendEmptyMessageDelayed(MESSAGE_AUTO_COMPLETE_TEXT_CHANGED, AUTO_COMPLETE_DELAY)
        }
        search_bar.setOnItemClickListener { adapterView, view, i, l ->
            val selectedPlace = viewModel.data.value?.getOrNull(i)
            if (selectedPlace != null) {
                viewModel.getPlaceDataForId(selectedPlace.id)
            }
        }

        img_bookmark.setOnClickListener {
            viewModel.saveToFavs { isUpdate ->
                runOnUiThread {
                    if (isUpdate)
                        Toast.makeText(this, "Place Updated!", Toast.LENGTH_LONG).show()
                    else
                        Toast.makeText(this, "Place Added!", Toast.LENGTH_LONG).show()
                }
            }
        }

        img_favs.setOnClickListener {
            viewModel.fetchFavs {
                runOnUiThread {
                    FavsDialog(this, ArrayList(it)) { place ->
                        viewModel.setCurPlace(place)
                    }.show()
                }
            }
        }

        btn_play.setOnClickListener {
            viewModel.resetDate()
        }
        btn_fwd.setOnClickListener {
            viewModel.addOrMinusDate(true)
        }
        btn_rev.setOnClickListener {
            viewModel.addOrMinusDate(false)
        }

        viewModel = ViewModelProviders.of(
            this, MapsViewModel.Factory(
                PlaceRepo(this),
                PhaseTimeCalc()
            )
        ).get(MapsViewModel::class.java)

        viewModel.data.observe(this, Observer {
            if (it != null)
                adapter.setData(it)
        })
        viewModel.selectedPlace.observe(this, Observer {
            if (it?.latLng != null) {
                showOnMap(it.latLng, it.name)
            }
        })
        viewModel.selectedDate.observe(this, Observer {
            if (it != null) {
                tv_datetime.text = sdf.format(it)
            }
        })

        viewModel.phaseTimeCalc.observe(this, Observer {
            if (it != null) {
                tv_sun_rise.text = String.format("%.2f", it.sunset)
                tv_sun_set.text = String.format("%.2f", it.sunrise)

                // TEMPORARY BECAUSE TIME CALCULATION IS WRONG =
                // ASSUME EVENT IS 75 SECONDS FROM CURRENT TIME
                val cal = Calendar.getInstance()
                cal.add(Calendar.SECOND, 75)
                // =================
                setGoldenHourReminder(cal.time)
            } else {
                tv_sun_rise.text = ""
                tv_sun_set.text = ""
            }
        })

        viewModel.errMsg.observe(this, Observer {
            if (it != null) {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            }
        })

        val marshmallowPermissions = MarshmallowPermissions(this)
        if (!marshmallowPermissions.checkPermissionForLocation()) {
            marshmallowPermissions.requestPermissionForLocation(REQUEST_CODE_FINE_LOCATION)
        } else {
            isLocationEnabled = true
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnMapClickListener {
            viewModel.setCurPlace(it)
        }

        markInitLocation()
    }

    private fun showOnMap(it: LatLng, name: String = "My Location") {
        myMarker?.remove()
        myMarker = mMap.addMarker(MarkerOptions().position(it).title(name))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(it))
        mMap.animateCamera(CameraUpdateFactory.zoomTo(16.0f))

        tv_latlng.text = Util.convertLatLngToString(it)
    }

    fun markInitLocation() {
        if (!::initLocation.isInitialized && ::mMap.isInitialized && isLocationEnabled) {
            val client = LocationServices.getFusedLocationProviderClient(this)
            client?.lastLocation?.addOnSuccessListener { lastLocation ->
                if (lastLocation != null) {
                    initLocation = LatLng(lastLocation.latitude, lastLocation.longitude)
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(initLocation))
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(16.0f))
                    viewModel.setCurPlace(initLocation)
                }
            }
        }
    }

    fun setGoldenHourReminder(date: Date) {
        if (!isGoldenHourReminderSet) {
            isGoldenHourReminderSet = true

            Util.saveStringPrefs(AppConstants.PREFS_GOLDEN_HOUR_TIME, date.time.toString())

            // Show reminder some time before event
            val reminderCal = Calendar.getInstance()
            reminderCal.time = date
            reminderCal.add(Calendar.SECOND, -AppConstants.REMINDER_SECONDS_BEFORE_EVENT)
            val timestampForReminder = reminderCal.time.time

            val delay = timestampForReminder - Calendar.getInstance().time.time
            if (delay > -AppConstants.REMINDER_MIN_SECONDS_BEFORE_EVENT) {
                if (delay > AppConstants.REMINDER_MIN_SECONDS_BEFORE_EVENT)
                    GoldenHourWorker.enque(delay)
                else
                    GoldenHourWorker.enque(AppConstants.REMINDER_MIN_SECONDS_BEFORE_EVENT)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        val isGranted: Boolean = grantResults.any { p -> p != PackageManager.PERMISSION_DENIED }
        if (isGranted) {
            if (requestCode == REQUEST_CODE_FINE_LOCATION) {
                isLocationEnabled = true
                markInitLocation()
            }
        }
    }


    val handler = Handler { msg ->
        if (msg.what == MESSAGE_AUTO_COMPLETE_TEXT_CHANGED) {
            viewModel.getPlacesByName(search_bar.text.toString())
        }
        return@Handler true
    }

    companion object {
        const val REQUEST_CODE_FINE_LOCATION = 1

        const val MESSAGE_AUTO_COMPLETE_TEXT_CHANGED = 10
        const val AUTO_COMPLETE_DELAY = 600L

        val sdf: SimpleDateFormat = SimpleDateFormat("E, dd MMM yyyy", Locale.ENGLISH)
    }
}
