package com.example.solarcalculator

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.app.Activity
import android.content.DialogInterface
import android.os.Build
import android.annotation.TargetApi
import androidx.appcompat.app.AlertDialog


class MarshmallowPermissions(val activity: AppCompatActivity) {

    fun checkPermissionForLocation(): Boolean {
        val result =
            ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
        return result == PackageManager.PERMISSION_GRANTED
    }

    fun requestPermissionForLocation(request_code: Int) {
        val alertBuilder = AlertDialog.Builder(activity)
        alertBuilder.setCancelable(true)
        alertBuilder.setTitle("Permission Required")
        alertBuilder.setMessage(activity.getString(R.string.loc_permission_req))
        alertBuilder.setPositiveButton(
            android.R.string.yes
        ) { dialog, which ->
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                request_code
            )
        }
        val alert = alertBuilder.create()
        alert.show()

    }
}