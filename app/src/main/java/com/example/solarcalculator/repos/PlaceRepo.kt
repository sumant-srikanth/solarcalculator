package com.example.solarcalculator.repos

import android.content.Context
import android.os.Parcel
import androidx.lifecycle.LiveData
import com.example.solarcalculator.R
import com.example.solarcalculator.db.AppDB
import com.example.solarcalculator.models.PlaceData
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.CancellationToken
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.*
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.google.android.libraries.places.api.net.PlacesClient
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class PlaceRepo(ctx: Context) {
    private val placesClient: PlacesClient
    private val execService: ExecutorService = Executors.newFixedThreadPool(1)

    init {
        if (!Places.isInitialized()) {
            Places.initialize(ctx.applicationContext, ctx.getString(R.string.google_maps_key));
        }
        placesClient = Places.createClient(ctx)
    }

    fun getPlaceDetails(
        placeId: String,
        onSuccess: (LatLng) -> Unit,
        onFailure: (Exception) -> Unit
    ) {
        placesClient.fetchPlace(object : FetchPlaceRequest() {
            override fun getPlaceId(): String {
                return placeId
            }

            override fun getSessionToken(): AutocompleteSessionToken? {
                return null
            }

            override fun getCancellationToken(): CancellationToken? {
                return null
            }

            override fun getPlaceFields(): MutableList<Place.Field> {
                return mutableListOf(Place.Field.ID, Place.Field.LAT_LNG)
            }
        }).addOnCompleteListener {
            if (it.isSuccessful) {
                val result = it.result?.place
                println(">>> FETCH PLACE DETAILS == ${it.result?.place}")
                if (result?.latLng != null) {
                    onSuccess(result.latLng!!)
                } else {
                    onFailure(Exception("Result was null"))
                }
            } else {
                onFailure(it.exception ?: Exception("Result was unsuccessful"))
            }
        }.addOnFailureListener { onFailure(it) }
    }

    fun getPlacesByName(
        name: String,
        onSuccess: (List<PlaceData>) -> Unit,
        onFailure: (Exception) -> Unit
    ) {
        placesClient.findAutocompletePredictions(object : FindAutocompletePredictionsRequest() {
            override fun getQuery(): String? {
                return name
            }

            override fun getSessionToken(): AutocompleteSessionToken? {
                return null
            }

            override fun getTypeFilter(): TypeFilter? {
                return null
            }

            override fun getLocationBias(): LocationBias? {
                return null
            }

            override fun getCancellationToken(): CancellationToken? {
                return null
            }

            override fun getLocationRestriction(): LocationRestriction? {
                return null
            }

            override fun getCountry(): String? {
                return null
            }

        }).addOnCompleteListener {
            if (it.isSuccessful) {
                val result = it.result?.autocompletePredictions
                if (result != null) {
                    result.forEach { p -> println(p.getFullText(null)) }
                    onSuccess(result.map { p ->
                        PlaceData(
                            p.placeId,
                            p.getPrimaryText(null).toString(),
                            p.getFullText(null).toString(),
                            LatLng(0.0, 0.0)
                        )
                    }.toList())
                } else {
                    onFailure(Exception("Result was null"))
                }
            } else {
                onFailure(it.exception ?: Exception("Result was unsuccessful"))
            }
        }.addOnFailureListener { onFailure(it) }
    }

    fun fetchFavs(onSuccess: (List<PlaceData>) -> Unit) {
        execService.execute {
            try {
                val result = AppDB.getInstance().getPinnedPlacesDao().getAll()
                onSuccess(result)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    fun saveFav(place: PlaceData, onSuccess: (Boolean) -> Unit) {
        execService.execute {
            try {
                val entry = place.copy()
                entry.created_at = Calendar.getInstance().time.time
                val isUpdate: Boolean
                if (AppDB.getInstance().getPinnedPlacesDao().insert(entry) < 0) {
                    AppDB.getInstance().getPinnedPlacesDao().update(entry)
                    isUpdate = true
                } else {
                    isUpdate = false
                }
                onSuccess(isUpdate)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

}