package com.example.solarcalculator

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat

class NotificationUtil {
    companion object {
        val CHANNEL_ID_GOLDEN_HOUR = "channel_golden_hour_reminder"
        val CHANNEL_NAME_GOLDEN_HOUR = "Golden Hour Reminder"
        val CHANNEL_DESC_GOLDEN_HOUR = "Shows notification reminder for Golden Hour"

        fun generateNotification(
            context: Context,
            info: String,
            title: String,
            msg: String,
            channelId: String,
            channelName: String,
            channelDesc: String,
            priority: Int
        ) {
            val notificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val builder = getNotificationBuilder(
                context,
                notificationManager, channelId, channelName, channelDesc, priority
            )

            val bigTextStyle = NotificationCompat.BigTextStyle()
            bigTextStyle.bigText(msg)
            builder.setStyle(bigTextStyle)

            val n = builder
                .setContentInfo(info)
                .setContentTitle(title)
                .setContentText(msg)
                .setSmallIcon(R.drawable.sun)
                .setAutoCancel(true)
                .setTimeoutAfter(10000)
                .setWhen(System.currentTimeMillis())
                .build()

            notificationManager.notify("SolarCalc", 1, n)
        }

        private fun getNotificationBuilder(
            ctx: Context,
            notificationManager: NotificationManager,
            channelId: String,
            channelName: String,
            channelDesc: String,
            priority: Int
        ): NotificationCompat.Builder {
            val builder = NotificationCompat.Builder(ctx, channelId)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val importance: Int
                if (priority == NotificationCompat.PRIORITY_HIGH) {
                    importance = NotificationManager.IMPORTANCE_HIGH
                } else {
                    importance = NotificationManager.IMPORTANCE_DEFAULT
                }

                var channel: NotificationChannel? =
                    notificationManager.getNotificationChannel(channelId)
                if (channel == null) {
                    channel = NotificationChannel(channelId, channelName, importance)
                }
                channel.enableVibration(true)
                channel.enableLights(true)
                channel.description = channelDesc
                try {
                    notificationManager.createNotificationChannel(channel)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            } else {
                builder.setDefaults(
                    NotificationCompat.DEFAULT_VIBRATE or NotificationCompat.DEFAULT_SOUND
                            or NotificationCompat.DEFAULT_LIGHTS
                )
                builder.priority = NotificationCompat.PRIORITY_HIGH
            }
            return builder
        }
    }
}